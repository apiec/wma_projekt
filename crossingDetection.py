import cv2
import os
import numpy as np
import imutils
import collections
import operator

# path to the folder containing analysed images
imgPath = './imgs'


# make a new folder in the img folder. returns path to the folder
def mkdirInImgPath(dirName):
    dirPath = os.path.join(imgPath, dirName)
    if not os.path.exists(dirPath):
        os.mkdir(dirPath)

    return dirPath


# mask everything that is not thought to be a road
def findRoad(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    _, sat, val = cv2.split(hsv)
    _, roadMask = cv2.threshold(sat, 40, 255, cv2.THRESH_BINARY_INV)
    roadMask = cv2.morphologyEx(roadMask, cv2.MORPH_OPEN, kernel=np.ones((11, 11)), iterations=1)
    roadMask = cv2.erode(roadMask, kernel=np.ones((3, 3)), iterations=1)
    cnts = cv2.findContours(roadMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    areaCntDict = {cv2.contourArea(c): c for c in cnts}
    roadCnt = areaCntDict[max(areaCntDict.keys())]
    roadMask = cv2.drawContours(np.zeros(roadMask.shape, dtype=np.uint8), [roadCnt], -1, 255, -1)
    avgVal = np.mean(np.concatenate(val))
    avgPixel = np.array([avgVal, avgVal, avgVal], dtype=np.uint8)
    newImg = img.copy()
    newImg[roadMask == 0] = avgPixel
    return newImg.astype(np.uint8)


# calculate distance from a line given by points A, B to point p
def distToLine(A, B, p):
    (x0, y0), (x1, y1), (x2, y2) = A, B, p
    return abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / np.sqrt(np.square(x2 - x1) + np.square(y2 - y1))


# get center of an opencv rect
def getRectCenter(rect):
    return rect[0] + rect[2] / 2, rect[1] + rect[3] / 2


# find unique subsets of approximately collinear points
# points can be put only into one of the subsets (no common points between the subsets)
def findCollinearPointClusters(pts: list):
    # for every pair of points in pts find all points that are approximately collinear with that pair
    allPointPairs = [(x, y) for x in pts for y in pts if x != y]
    collinearDict = {pp: set() for pp in allPointPairs}
    for A, B in allPointPairs:
        collinearDict[(A, B)].add(A)
        collinearDict[(A, B)].add(B)
        for p in pts:
            if p != A and p != B and distToLine(A, B, p) < 10:
                collinearDict[(A, B)].add(p)
    # sets are not hashable so they need to be converted to tuples
    # before using them in a dict with collections.Counter
    # also get rid of clusters smaller than 4
    allClusters = [tuple(v) for v in collinearDict.values() if len(v) > 3]
    permutationCounter = collections.Counter(allClusters)
    # get the cluster that occurs the most in the list
    # it is most probably has no (or the least) outliers
    # then remove all clusters that share any of the points with that cluster
    # repeat until no clusters are left
    uniquePointClusters = []
    while permutationCounter:
        maxOccurrencePermutation = max(permutationCounter, key=permutationCounter.get)
        uniquePointClusters.append(maxOccurrencePermutation)
        for center in maxOccurrencePermutation:
            for k in permutationCounter.copy().keys():  # making a copy because the counter gets modified in the loop
                if center in k:
                    permutationCounter.pop(k)

    return uniquePointClusters


# find groups of contours that are approximately collinear
# using the findCollinearPointClusters
def findCollinearContourGroups(cnts):
    centerContourDict = {getRectCenter(cv2.boundingRect(c)): c for c in cnts}
    centers = [getRectCenter(cv2.boundingRect(c)) for c in cnts]
    collinearCenterClusters = findCollinearPointClusters(centers)
    return [[centerContourDict.get(center) for center in cluster] for cluster in collinearCenterClusters]


def onEveryImage(img):
    onEveryImage.callCounter += 1
    allResultsFolder = mkdirInImgPath('results')
    # image preprocessing
    # rescaling
    targetDim = 2000
    y, x = img.shape[:2]
    scalingFactors = (1, y/x) if x > y else (x/y, 1)
    newDims = tuple(int(targetDim * sf) for sf in scalingFactors)
    resized = cv2.resize(img, newDims)
    # focus on the road
    roadMasked = findRoad(resized)
    # grayscale
    gray = cv2.cvtColor(roadMasked, cv2.COLOR_BGR2GRAY)
    # eqHist = cv2.equalizeHist(gray)
    blurred = cv2.medianBlur(gray, 5)
    blurred = cv2.bilateralFilter(blurred, 5, 20, 5)
    eqHist = cv2.normalize(blurred, None, alpha=0, beta=1.1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    eqHist = np.clip(255 * eqHist, 0, 255).astype(np.uint8)
    # threshold
    _, thresh = cv2.threshold(eqHist, 210, 255, cv2.THRESH_BINARY)
    morphed = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel=np.ones((5, 5)), iterations=1)
    morphed = cv2.morphologyEx(morphed, cv2.MORPH_CLOSE, kernel=np.ones((5, 5)), iterations=1)
    morphed = cv2.erode(morphed, kernel=np.ones((3, 3)), iterations=1)
    # extract contours
    cnts = cv2.findContours(morphed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = [cv2.convexHull(c) for c in cnts]
    cnts = [c for c in cnts
            # find contours that are approximately a quadrangle
            if len(cv2.approxPolyDP(c, np.clip(0.03 * cv2.arcLength(c, True), 0, 10), True)) < 6
            # dismiss small contours
            and 1000 < cv2.contourArea(c)]

    colCntGroups = findCollinearContourGroups(cnts)
    for c in colCntGroups:
        allPts = np.concatenate(c)
        cv2.drawContours(resized, [cv2.convexHull(allPts)], -1, (0, 255, 0), 4)

    cv2.imwrite(os.path.join(allResultsFolder, str(onEveryImage.callCounter) + '.png'), resized)

    # debugging below
    contoursOnly = np.zeros(resized.shape, dtype=np.uint8)
    contoursWithRects = np.zeros(resized.shape, dtype=np.uint8)
    cv2.drawContours(contoursOnly, cnts, -1, (255, 255, 255), 4)
    cv2.drawContours(contoursWithRects, cnts, -1, (255, 255, 255), 4)
    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        center = tuple(int(p) for p in getRectCenter(cv2.boundingRect(c)))
        cv2.line(contoursWithRects, center, center, (0, 0, 255), 20)
        cv2.rectangle(contoursWithRects, (x, y), (x + w, y + h), (128, 128, 128), 4)

    for ccg in colCntGroups:
        centers = [tuple(int(p) for p in getRectCenter(cv2.boundingRect(c))) for c in ccg]
        centers = np.array(centers)
        vx, vy, _, _ = cv2.fitLine(centers, cv2.DIST_L2, param=0, reps=0.01, aeps=0.01)
        x, y, w, h = cv2.boundingRect(centers)
        cc = getRectCenter((x, y, w, h))
        lineLen = np.sqrt(w * w + h * h) / 2
        p1 = tuple(map(operator.add, cc, (vx * lineLen, vy * lineLen)))
        p2 = tuple(map(operator.add, cc, (-vx * lineLen, -vy * lineLen)))
        cv2.line(contoursWithRects, p1, p2, (0, 0, 255), 8)

    # create a subfolder for the photo and save some intermediary images
    pathName = mkdirInImgPath(str(onEveryImage.callCounter))

    cv2.imwrite(os.path.join(pathName, 'contoursOnly.png'), contoursOnly)
    cv2.imwrite(os.path.join(pathName, 'contoursWithRects.png'), contoursWithRects)
    cv2.imwrite(os.path.join(pathName, 'final.png'), resized)
    cv2.imwrite(os.path.join(pathName, 'gray.png'), gray)
    cv2.imwrite(os.path.join(pathName, 'eqHist.png'), eqHist)
    cv2.imwrite(os.path.join(pathName, 'binary.png'), morphed)
    cv2.imwrite(os.path.join(pathName, 'masked.png'), roadMasked)


######################
# END OF DEFINITIONS #
######################


onEveryImage.callCounter = 0

fileNames = [f for f in os.listdir(imgPath) if os.path.isfile(os.path.join(imgPath, f))]
images = [cv2.imread(os.path.join(imgPath, f)) for f in fileNames]
for img in images:
    onEveryImage(img)
